#/bin/bash

echo "Check all tool archives that have a version svcomp24 or testcomp24 with an archive DOI."

echo
echo "The following tools have a version for SV-COMP 2024 with DOI:"
TOOLS_SVCOMP=$(for TOOLFILE in data/*.yml; do
    TOOL=$(basename "$TOOLFILE" ".yml")
    yq --raw-output ".versions [] | select(.version == \"svcomp24\"   and .doi != null) | \"$TOOL\"" $TOOLFILE
done)
echo $TOOLS_SVCOMP

echo
echo "The following tools have a version for Test-Comp 2024 with DOI:"
TOOLS_TESTCOMP=$(for TOOLFILE in data/*.yml; do
    TOOL=$(basename "$TOOLFILE" ".yml")
    yq --raw-output ".versions [] | select(.version == \"testcomp24\" and .doi != null) | \"$TOOL\"" $TOOLFILE
done)
echo $TOOLS_TESTCOMP

echo
echo "Check archives of changed tool files:"
for TOOLFILE in $(git diff --name-only --diff-filter=d origin/main -- data/*.yml); do
    TOOL=$(basename "$TOOLFILE" ".yml")
    echo "Checking $TOOL"
    TOOL_DIR=$(mktemp --directory --tmpdir "check-archives-$TOOL-XXXXXXXXXX")
    mkdir "$TOOL_DIR"/cache
    mkdir "$TOOL_DIR"/2024
    # TODO: Download competition-specific
    echo "Downloading archive for $TOOL into $TOOL_DIR"
    "$(dirname "$0")"/../scripts/execute_runs/update-archives.py \
        --fm-root "$(dirname "$0")"/../data/ \
        --archives-root "$TOOL_DIR" \
        $TOOL
    if [[ $TOOLS_SVCOMP =~ "$TOOL" ]]; then
      echo "Checking archive for $TOOL in $TOOL_DIR for SV-COMP"
      "$(dirname "$0")"/../scripts/test/check-archive.py svcomp "$TOOL_DIR/2024/$TOOL.zip"
    fi
    if [[ $TOOLS_TESTCOMP =~ "$TOOL" ]]; then
      echo "Checking archive for $TOOL in $TOOL_DIR for Test-Comp"
      "$(dirname "$0")"/../scripts/test/check-archive.py testcomp "$TOOL_DIR/2024/$TOOL.zip"
    fi
    rm "$TOOL_DIR/2024/$TOOL.zip"
    rmdir "$TOOL_DIR"/2024
    rm "$TOOL_DIR"/cache/[0-9]*[0-9].zip
    rmdir "$TOOL_DIR"/cache
    rmdir "$TOOL_DIR"
done
